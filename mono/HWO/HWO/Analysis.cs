﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWO
{

    class Analysis
    {
        Double v;        
        Double a;
        Double vMax;
        Double aMax;
        PiecePosition oldPosition;
        Double curThrottle;
        Double carAngle;
        Piece oldPiece;
        System.IO.StreamWriter file;
        public Analysis()
        {
            file = new System.IO.StreamWriter("G:\\Git\\hwo2014-team-2017\\mono\\HWO\\teste.csv");
            v = 0;
            a = 0;
            vMax = 0;
            aMax = 0;
        }

        public void update(Position position, double throttle, Piece curPiece)
        {
            PiecePosition currentCarPosition = position.piecePosition;
            Double vCur = 0;
            curThrottle = throttle;

            if (oldPosition == null)
            {
                oldPiece = curPiece;
                oldPosition = currentCarPosition;
            }

            if (currentCarPosition.pieceIndex == oldPosition.pieceIndex)
            {
                vCur = currentCarPosition.inPieceDistance - oldPosition.inPieceDistance;
            }
            else if(oldPiece.length!=0)
            {
                vCur = (oldPiece.length - oldPosition.inPieceDistance) + currentCarPosition.inPieceDistance;
            }
            else
            {
                vCur = ((Math.PI * oldPiece.radius * oldPiece.angle / 180) - oldPosition.inPieceDistance) + currentCarPosition.inPieceDistance;
            }

            a = vCur - v;
            v = vCur;
            oldPosition = currentCarPosition;
            carAngle = position.angle;
            oldPiece = curPiece;

            if (v > vMax)
                vMax = v;
            if (a > aMax)
                aMax = a;
        }

        public void printHeader()
        {
            //System.Console.WriteLine(" V;A;Piece;Vmax;Amax;Angle;Throttle;Piece Angle; Piece Radius;");
            file.WriteLine(" V;A;Piece;Vmax;Amax;Angle;Throttle;Piece Angle; Piece Radius;");

        }

        public void printStatistics()
        {
            /*System.Console.WriteLine(String.Format("{0:00.000}", v) + ";" +
                                     String.Format("{0:00.000}", a) + ";" +
                                     oldPosition.pieceIndex + ";" + 
                                     String.Format("{0:00.000}", vMax) + ";" +
                                     String.Format("{0:00.000}", aMax) + ";" +
                                     String.Format("{0:00.000}", carAngle) + ";" +
                                     String.Format("{0:0.00}", curThrottle) + ";" +
                                     String.Format("{0:0.00}", oldPiece.angle) + ";" +
                                     String.Format("{0:0.00}", oldPiece.radius) + ";");*/
            file.WriteLine(String.Format("{0:00.000}", v) + ";" +
                  String.Format("{0:00.000}", a) + ";" +
                  oldPosition.pieceIndex + ";" +
                  String.Format("{0:00.000}", vMax) + ";" +
                  String.Format("{0:00.000}", aMax) + ";" +
                  String.Format("{0:00.000}", carAngle) + ";" +
                  String.Format("{0:0.00}", curThrottle) + ";" +
                  String.Format("{0:0.00}", oldPiece.angle) + ";" +
                  String.Format("{0:0.00}", oldPiece.radius) + ";");
        }

        public void end()
        {
            file.Close();
        }

    }
}
