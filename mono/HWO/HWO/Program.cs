﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Linq;

namespace HWO
{
    public class Bot
    {
        // <AK>
        Race race = new Race();
        CarPositions carPositions = new CarPositions();
        double carSpeed = 0;
        // </AK>

        public static void Main(string[] args)
        {
            string host = args[0];
            int port = int.Parse(args[1]);
            string botName = args[2];
            string botKey = args[3];
            string trackName = "";
            int carCount = 1;
            if (args.Length > 3)
                trackName = args[4];
            if (args.Length > 4)
                carCount = int.Parse(args[5]);

            Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

            using (TcpClient client = new TcpClient(host, port))
            {
                NetworkStream stream = client.GetStream();
                StreamReader reader = new StreamReader(stream);
                StreamWriter writer = new StreamWriter(stream);
                writer.AutoFlush = true;

                if (args.Length <= 3)
                    new Bot(reader, writer, new Join(botName, botKey));
                else
                    new Bot(reader, writer, new CreateRace(botName, botKey, trackName, carCount));
            }
        }

        private StreamWriter writer;

        Bot(StreamReader reader, StreamWriter writer, Join join)
        {
            this.writer = writer;
            string line;

            send(join);

            while ((line = reader.ReadLine()) != null)
            {
                MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
                switch (msg.msgType)
                {
                    case "carPositions":

                        // <AK>
                        carPositions = Parce.parteCarPositions(msg.data);

                        carSpeed = BotActions.takeAction(race, carPositions);

                        send(new Throttle(carSpeed));
                        // </AK>

                        break;
                    case "join":
                        Console.WriteLine("Joined");
                        send(new Ping());
                        break;
                    case "gameInit":
                        Console.WriteLine("Race init");

                        // <AK>
                        race = Parce.parceRace(msg.data.ToString());
                        // </AK>

                        send(new Ping());
                        break;
                    case "gameEnd":
                        Console.WriteLine("Race ended");
                        send(new Ping());
                        break;
                    case "gameStart":
                        Console.WriteLine("Race starts");
                        send(new Ping());
                        break;
                    default:
                        send(new Ping());
                        break;
                }
            }
        }

        Bot(StreamReader reader, StreamWriter writer, CreateRace createRace)
        {
            this.writer = writer;
            string line;
            Analysis analysis = new Analysis();

            send(createRace);

            analysis.printHeader();
            while ((line = reader.ReadLine()) != null)
            {
                MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
                switch (msg.msgType)
                {
                    case "carPositions":

                        // <AK>
                        carPositions = Parce.parteCarPositions(msg.data);
                        Position p = carPositions.positions.Find(x => x.id.name == "LIGHTNINGMCQUEEN");
                        analysis.update(p, carSpeed, race.track.pieces[p.piecePosition.pieceIndex]);
                        analysis.printStatistics();

                        carSpeed = BotActions.takeAction(race, carPositions);

                        /*Console.WriteLine(carPositions.positions.Find(x => x.id.name == "LIGHTNINGMCQUEEN").piecePosition.lap.ToString() + " " + 
                            carPositions.positions.Find(x => x.id.name == "LIGHTNINGMCQUEEN").piecePosition.pieceIndex.ToString() + " " +
                            carPositions.positions.Find(x => x.id.name == "LIGHTNINGMCQUEEN").angle.ToString() + " " +
                            carSpeed.ToString());*/

                        send(new Throttle(carSpeed));
                        // </AK>
                        break;

                    case "join":
                        Console.WriteLine("Joined");
                        send(new Ping());
                        break;
                    case "createRace":
                        Console.WriteLine("Race created");
                        send(new Ping());
                        break;
                    case "gameInit":
                        Console.WriteLine("Race init");
                        // <AK>
                        race = Parce.parceRace(msg.data.ToString());
                        // </AK>
                        send(new Ping());
                        break;
                    case "gameEnd":
                        Console.WriteLine("Race ended");
                        analysis.end();
                        send(new Ping());
                        break;
                    case "gameStart":
                        Console.WriteLine("Race starts");
                        send(new Ping());
                        break;
                    default:
                        send(new Ping());
                        break;
                }
            }
        }

        private void send(SendMsg msg)
        {
            writer.WriteLine(msg.ToJson());
        }
    }

    public class BotActions
    {
        CarPositions positions { get; set; }
        Race race { get; set; }

        public static double takeAction(Race race, CarPositions carPositions)
        {
            int currentPosition = carPositions.positions.Find(x => x.id.name == "LIGHTNINGMCQUEEN").piecePosition.pieceIndex;
            float carAngle = carPositions.positions.Find(x => x.id.name == "LIGHTNINGMCQUEEN").angle;
            double carSpeed = 1;

            if(Math.Abs(carAngle) > 30 && race.track.pieces[currentPosition].angle!=0)
            {
                carSpeed = 0.1;
            }
            else
            {
                carSpeed = 1;
            }


            /*
            if (nextPieceIsTurn(race, currentPosition))
            {
                if (Math.Abs(carAngle) > 30)
                    carSpeed = 0.1;
                else
                    carSpeed = 0.2;
            }
            else
            {
                if (nextPieceIsTurn(race, currentPosition + 1))
                    carSpeed *= 0.5;
            }*/

      

            return carSpeed;
        }

        public void changeSpeed()
        {

        }

        public void changeLane()
        {

        }

        public static bool nextPieceIsTurn(Race race, int currentPosition)
        {
            int raceLength = race.track.pieces.Count;
            int nextPosition = currentPosition + 1;

            if (nextPosition >= raceLength)
                nextPosition = 0;

            return (race.track.pieces[nextPosition].length == 0.0);
        }



        public void useTurbo()
        {

        }
    }

    class MsgWrapper
    {
        public string msgType;
        public Object data;

        public MsgWrapper(string msgType, Object data)
        {
            this.msgType = msgType;
            this.data = data;
        }
    }

    abstract class SendMsg
    {
        public string ToJson()
        {
            return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
        }
        protected virtual Object MsgData()
        {
            return this;
        }

        protected abstract string MsgType();
    }

    class Join : SendMsg
    {
        public string name;
        public string key;
        public string color;

        public Join(string name, string key)
        {
            this.name = name;
            this.key = key;
            this.color = "red";
        }

        protected override string MsgType()
        {
            return "join";
        }
    }

    class CreateRace : SendMsg
    {
        public BotId botId;
        public string trackName;
        public int carCount;

        public CreateRace(string name, string key, string trackName, int carCount)
        {
            this.botId = new BotId();
            this.botId.name = name;
            this.botId.key = key;
            this.trackName = trackName;
            this.carCount = carCount;
        }

        protected override string MsgType()
        {
            return "createRace";
        }
    }

    class Ping : SendMsg
    {
        protected override string MsgType()
        {
            return "ping";
        }
    }

    class Throttle : SendMsg
    {
        public double value;

        public Throttle(double value)
        {
            this.value = value;
        }

        protected override Object MsgData()
        {
            return this.value;
        }

        protected override string MsgType()
        {
            return "throttle";
        }
    }

    // <AK>

    // Classes JSON in C#
    public class Race
    {
        public Track track { get; set; }
        public string gameId { get; set; }
    }

    public class Track
    {
        public string id { get; set; }
        public string name { get; set; }
        public List<Piece> pieces { get; set; }
    }

    public class Piece
    {
        public float length { get; set; }
        public Boolean switchs { get; set; }
        public float radius { get; set; }
        public float angle { get; set; }
    }

    public class CarPositions
    {
        public List<Position> positions { get; set; }
    }

    public class Position
    {
        public Id id { get; set; }
        public float angle { get; set; }
        public PiecePosition piecePosition { get; set; }
    }

    public class Id
    {
        public string name { get; set; }
        public string color { get; set; }
    }

    public class PiecePosition
    {
        public int pieceIndex { get; set; }
        public double inPieceDistance { get; set; }
        public Lane lane { get; set; }
        public int lap { get; set; }
    }

    public class Lane
    {
        public int startLaneIndex { get; set; }
        public int endLaneIndex { get; set; }
    }

    // <YSP>
    public class BotId
    {
        public string name { get; set; }
        public string key { get; set; }
    }
    // </YSP>

    // Parcing JSON to C#
    class Parce
    {
        public static Race parceRace(string _raceJSON)
        {
            return JsonConvert.DeserializeObject<Race>(JObject.Parse(_raceJSON)["race"].ToString());
        }

        public static CarPositions parteCarPositions(object _carsPositionsJSON)
        {
            return new CarPositions() { positions = JsonConvert.DeserializeObject<List<Position>>(_carsPositionsJSON.ToString()) };
        }
    }

    // </AK>
}
